import { Component, OnInit } from '@angular/core';
import { EventService } from './shared/event.service';
import { ToastrService } from '../common/toastr.service';
import { IEvent } from './shared/index';

@Component({
    template: `
    <div>
        <h1>Upcoming Angular Events</h1>
        <hr/>
        <div class="row">
            <div *ngFor="let event of events" class="col-md-5">
                <event-thumbnail #thumbnail (click)="handleThumbnailClick(event.name)" [event]="event"></event-thumbnail>
                <!--<button class="btn btn-primary" (click)="thumbnail.logFoo()">Log some foo</button>-->
            </div>
           
        </div>
    </div>
    `
})
export class EventsListComponent implements OnInit {
    events:IEvent[]

    constructor(private eventService: EventService, private toastr: ToastrService) {
     
    }

    ngOnInit() {
       this.events = this.eventService.getEvents();
    }

   /* handleEventClicked(data) {
        
    }*/

    handleThumbnailClick(eventName) {
      this.toastr.success(eventName)
    }
}